#!/usr/bin/env ruby
if ARGV.size != 1
  puts "Solo se pueden emplear 1 argumento"
  puts "Usar el programa 'create-users'"
  puts "filename : nombre de fichero"
  exit 1
end

filename = ARGV[0]
content = `cat #{filename}`
lines   = content.split("\n")
user = `whoami`
user = user.chomp
if user == "root"
	lines.each do |row|
		system ("useradd -d /home/#{row} -m #{row}")
        	system ("passwd #{row}")
       		system ("mkdir /home/#{row}/private")
        	system ("mkdir /home/#{row}/group")
        	system ("mkdir /home/#{row}/public")
        	system ("chmod 700 /home/#{row}/private")
        	system ("chmod 750 /home/#{row}/group")
        	system ("chmod 755 /home/#{row}/public")

  	end
else
	puts "Se necesita ser root."
	exit 1
end
