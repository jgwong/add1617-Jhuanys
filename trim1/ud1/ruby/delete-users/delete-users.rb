#!/usr/bin/ruby

if ARGV.size != 1
  puts "Solo se pueden emplear 1 argumento"
  puts "Usar el programa 'delete-users'"
  puts "filename : nombre de fichero"
  exit 1
end

filename = ARGV[0]
content = `cat #{filename}`
lines   = content.split("\n")

lines.each do |row|
  puts "Procesando: #{row}"

  system ("userdel #{filename}")
end
