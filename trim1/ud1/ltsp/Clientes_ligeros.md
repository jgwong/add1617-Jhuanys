

*Jhuanys Gil Wong*
*2º ASIR*


----------


##	Creando clientes ligeros
Para comenzar la práctica deberemos asignarle a nuestra máquina un nombre de host referido a nuestro primer apellido y además, establecer una IP estática tanto en el puerto de red interna como en el modo puente.
Una vez hayamos realizado estas tareas, introducimos los comandos que se reflejan a continuación para comprobar que lo hemos hecho correctamente. 

![comprobacion1](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/87beb439b5b17b4c3783a7e7cc925907170d13c6/trim1/ud1/ltsp/images/comprobacion1.png?token=18cedcf4466e2dbd5f06f50ae541cf851bf1fdee)


![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/87beb439b5b17b4c3783a7e7cc925907170d13c6/trim1/ud1/ltsp/images/comprobacion2.png?token=54e4922bf013e83477f2c1fe48064999d8b0139e)

Una vez realizadas estas comprobaciones, podemos proceder a instalar el paquete "openssh-server" usando el comando apt-get install:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/87beb439b5b17b4c3783a7e7cc925907170d13c6/trim1/ud1/ltsp/images/instalar_ssh_server.png?token=f30e478388a475ac0a8f65a614eb32c3fd2e62a7)

También para cualquier problema o en prácticas futuras, es conveniente habilitar la opción de permitrootlogin en el ssh para ello vamos a la ubicación /etc/ssh/ y modificamos con un editor de texto el archivo sshd_config

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/87beb439b5b17b4c3783a7e7cc925907170d13c6/trim1/ud1/ltsp/images/permitrootlogin_yes.png?token=f7b2ec8b1046a6da760a8eb0346939e878654ee6)

Ahora que hemos hecho los preliminares podemos instalar el paquete "ltsp-server-standalone" que nos ayudará a crear la imagen de nuestro propio sistema:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/87beb439b5b17b4c3783a7e7cc925907170d13c6/trim1/ud1/ltsp/images/instalar_ltsp_server.png?token=3d850437c41d698ad49a0933069dfb449e16f729)

Creamos la imagen del sistema...

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/87beb439b5b17b4c3783a7e7cc925907170d13c6/trim1/ud1/ltsp/images/construir_imagen_so.png?token=77051ac9d7c2d252393bb9433491f517efaf43d9)

Y para comprobar que la ha realizado, con el comando ltsp-info nos informará.
Es aconsejable realizar esta comprobación ya que a veces por falta de espacio en el disco duro nos puede salir un error al intentar hacer la imagen y no darnos cuenta.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/87beb439b5b17b4c3783a7e7cc925907170d13c6/trim1/ud1/ltsp/images/ltsp_info.png?token=0199c7b393a510849c2316d41cd27463aeaf358a)

Con un editor de texto, en este caso nano, modificamos el archivo isc-dhcp-server para añadirle en el apartado "INTERFACES" la interfaz de red que está conectada a la red interna, ya que es la que usará nuestro cliente.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/87beb439b5b17b4c3783a7e7cc925907170d13c6/trim1/ud1/ltsp/images/asignar_interfaz_dhcp.png?token=cc491c98e3e1ef16ee459acf372fea1b97b3abd7)

Ahora vamos al archivo /etc/ltsp/dhcpd.conf y lo modificamos también pero solo cambiamos las palabras que digan i386 por amd64, con esto ya nos debería funcionar.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/87beb439b5b17b4c3783a7e7cc925907170d13c6/trim1/ud1/ltsp/images/comprobar_rutas.png?token=b48cecbf436c2c7e0103ea8af31f5df63f9d7e62)

Para comprobar que efectivamente funciona, introducimos el comando "/etc/init.d/isc-dhcp-server status y como se muestra, está activo:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/87beb439b5b17b4c3783a7e7cc925907170d13c6/trim1/ud1/ltsp/images/estado_dhcp.png?token=65e09cff01317c80f696e63e64174f6bd0bbc4cc)

En este paso crearemos una máquina Ubuntu cliente pero sin disco duro y que arranque por defecto por la interfaz de red en modo interno:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/87beb439b5b17b4c3783a7e7cc925907170d13c6/trim1/ud1/ltsp/images/config_maquinacliente.png?token=43f8e353107f017174aaaf21ea8f57fbbfd4d8ea)

Tras pasar unos minutos, en la máquina cliente nos debería aparecer una ventana similar a esta, en la que introduciremos los datos de algún usuario de nuestra máquina servidor:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/87beb439b5b17b4c3783a7e7cc925907170d13c6/trim1/ud1/ltsp/images/autenticacion_en_cliente.png?token=86eef9d081c10cb31683a58851c7d521a7a020b5)

Una vez dentro nos aparecerá este mensaje, en el que pondremos nuestra clave nuevamente para continuar.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/87beb439b5b17b4c3783a7e7cc925907170d13c6/trim1/ud1/ltsp/images/entrando_en_ubuntu_cliente.png?token=ea38f229618eb3cc2711d729a31d81e2ca574308)

Y por último podemos observar que estamos en distintos usuarios en distintas máquinas, pero ambos creados en el mismo equipo, en el servidor.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/87beb439b5b17b4c3783a7e7cc925907170d13c6/trim1/ud1/ltsp/images/diferentes_usuarios.png?token=7a7f894a9aa39731d5608e5307e3f0fe33ed06bf)