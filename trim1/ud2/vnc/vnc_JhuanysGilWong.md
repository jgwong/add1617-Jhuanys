Jhuanys Gil Wong.
2º ASIR


----------


#Conexión remota con VNC

Para comenzar esta práctica necesitaremos descargar desde la página web el paquete de VNC server en nuestra máquina Windows 2012 Server:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/a9070ff239b64fce04997ae62d0e806530c8eca3/trim1/ud2/vnc/images/vnc_server_windows.png?token=96d6278d6ed4ef35366201a4ffabf12fede6dc4d)

Una vez seleccionado el instalador x64 bits lo descargamos e instalamos.
Al tener el programa ya instalado, añadiremos las IP's que podrán acceder en "Access Control > New IP Access Rule", no debemos olvidar marcar la opción "Allow" para permitir el acceso

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/a9070ff239b64fce04997ae62d0e806530c8eca3/trim1/ud2/vnc/images/habilitar_acceso_wserver.png?token=a5415c25b4220310902e7e4db0c3024450978407)

Instalamos el paquete VNC viewer en una máquina Windows 7 para acceder de forma remota al servidor:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/a9070ff239b64fce04997ae62d0e806530c8eca3/trim1/ud2/vnc/images/web_vnc_viewer.PNG?token=e4d8dcd29a6e8acad4a40d24f03d3621737b96d9)

Y una vez instalado abrimos la aplicación e intentamos acceder de forma remota al Windows 2012 Server introduciendo su IP y el puerto de conexión, en este caso el 5900:

 ![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/a9070ff239b64fce04997ae62d0e806530c8eca3/trim1/ud2/vnc/images/windows_server_a_w7.png?token=b971c9b39a61fb53388cef24754ea5ea7f7c942c)

Como podemos observar en la anterior imagen, estamos dentro de la máquina de Windows 2012 Server de forma remota y podemos navegar dentro de ella como lo haríamos normalmente.

Ahora intentemos hacer lo mismo pero desde una máquina cliente de OpenSUSE.
Para ello descargamos e instalamos el paquete krdc desde los repositorios:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/a9070ff239b64fce04997ae62d0e806530c8eca3/trim1/ud2/vnc/images/krdc_client_suse.PNG?token=6b1754a87483196b4469de5d01310ac46c066a8c)

Una vez instalado abrimos este programa e introducimos la IP del Windows 2012 Server y su puerto de conexión tal y como lo hicimos en el Windows 7:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/a9070ff239b64fce04997ae62d0e806530c8eca3/trim1/ud2/vnc/images/windows_server_suse_client.png?token=159643662e3b523dadd343d021a868d882932903)

Podemos observar que también hemos accedido y podemos manipular esta máquina remotamente.

Llegados a este punto, accederemos a el servidor OpenSUSE desde una máquina cliente también del mismo S.O.
Para ello necesitaremos ir a "Yast > Administración remota (VNC)":


![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/a9070ff239b64fce04997ae62d0e806530c8eca3/trim1/ud2/vnc/images/yast_vnc.PNG?token=05fc3b36c0a8e2553cc62abba3bfb473a5209718)

Ahora nos dará a elegir si deseamos permitir la administración remota o no.
También nos permite añadir una excepción al cortafuegos pero en este caso ya lo tenemos deshabilitado.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/a9070ff239b64fce04997ae62d0e806530c8eca3/trim1/ud2/vnc/images/acceso_remoto_vnc_server_suse.PNG?token=17349320821df5a63da015205b91eda654005e70)

Ahora instalaremos el paquete que necesitaremos para que el VNC funcione correctamente:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/a9070ff239b64fce04997ae62d0e806530c8eca3/trim1/ud2/vnc/images/paquete_necesario_vnc_server_suse.PNG?token=150035074b743805cbe6c5c5ff14d30feb323966)

Ahora con la herramienta que hemos instalado anteriormente "krdc" accederemos desde el OpenSUSE cliente al servidor.
Introducimos la IP del servidor y el puerto correspondiente, en este caso el "5901":

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/a9070ff239b64fce04997ae62d0e806530c8eca3/trim1/ud2/vnc/images/conectar_suse_client_suse_server.PNG?token=7fc442f3066a61bd6c8f76502b20be63447680a5)


![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/a9070ff239b64fce04997ae62d0e806530c8eca3/trim1/ud2/vnc/images/conexion_establecida_suses.PNG?token=960e670d7f1b298c7daa47e4f48d6cd6f2e7c0ce)

Por último accederemos al OpenSUSE server desde el cliente de Windows 7 empleando las mismas herramientas y cambiando únicamente la IP y el puerto:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/a9070ff239b64fce04997ae62d0e806530c8eca3/trim1/ud2/vnc/images/conexion_establecida_suse_windows7.PNG?token=f3a4bcb1a70b3de5e8ae71332be2b14e0939d238)