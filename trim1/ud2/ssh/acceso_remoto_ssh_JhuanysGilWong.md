

*Jhuanys Gil Wong*
*2º ASIR*


----------


##	Acceso remoto con SSH

Para comenzar configuraremos los parámetros de conexión de nuestras máquinas comenzando por la ssh-server y continuando con las clientes.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/combandos%20_comp_inicial.png?token=19e6538c132810666b1175a899deaebd7e79d204)

Para comprobar que están bien configuradas deberán hacer ping entre todas ellas y por sus nombres de hosts.
Para ello modificaremos el archivo /etc/hosts de las máquinas openSUSE y Windows.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/conf_etc_hosts.png?token=15d3d682ab29f1358c1daf5b173dc5f43c584c8e)

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/etc_hosts_w7.png?token=aa95637d3702e75cc1d53e868f69408ce5f1cd09)

Una vez hayamos realizado este paso podremos hacer ping mediante sus nombres de hosts desde cualquiera de las máquinas como se muestra a continuación:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/ping_clients.png?token=055594be40fe92532a76a1cc68a4fab0af6fcf1b)

Ahora en la máquina ssh-server crearemos 4 usuarios con el primer apellido del alumno1, 2, 3 y 4.
Para ello usaremos la herramienta YAST.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/crear_usus.png?token=d219f9ba555cdd1d3e04c622fda8b567f0d3feee)

Una vez creados estos usuarios instalaremos la herramienta que nos permitirá conectarnos via ssh de clientes al servidor (openssh) que la descargaremos desde los repositorios del sistema.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/instalar_openssh.png?token=549ac73e1a724213ecca90f9df4eed4417ff7d77)

Y una ves instalado el paquete comprobamos que está en funcionamiento con el comando "systemctl status sshd" y que está escuchando las peticiones en el puerto 22.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/status_sshd.png?token=4569e53032301def045cdb0aacabb36acca07b6b)

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/escucha_p22.png?token=e6f4936c1c73f17aba0335134d440a7665bb0d7c)

Ahora desde el cliente de openSUSE con el comando nmap haremos un rastreo de puerto de la máquina servidor, para comprobar que también efectivamente escucha en el puerto 22.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/comprobacion_nmap.png?token=a9f9d5817e8c6792f5396e1a18fbbfd07c54b869)

Pero para que el servicio ssh funcione sin problemas deberemos habilitar la opción que permita al cortafuegos dejar pasar las comunicaciones de este servicio.
Para ello vamos a la herramienta YAST en ambos openSUSES y le añadimos una excepción:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/excesion_firewall.png?token=25a2cc04d7026b6d8a926084dca541d80270366b)

Ahora bien, para comprobar que todo ha está correctamente deberemos probar la conexión ssh.
Para ello nos conectaremos desde las máquinas clientes al usuario apellido1 alojado en el servidor.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/conectar_usu1_ssh_suse.png?token=0f95f8493325098dfed2a584982493c350edcd6a)

Hay que tener en cuenta que en Windows deberemos instalar el programa PuTTy para conectarnos via ssh, lo descargamos desde la web oficial y lo ejecutamos.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/putty_w7_usu1.png?token=2d1f7ef5a04a4cdbdcbc93d9b206cd16067536e0)

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/conectar_usu1_putty.png?token=d403c1005d0290a115fb807e04c16cb951c29d05)

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/putty_w7_usu2.png?token=d4ba75ac23fd6e3a91e839126e08e899d78675b7)

Una vez que hayamos accedido al usuario via ssh en el cliente SUSE se nos creará una carpeta denominada "know_hosts" donde se alojarán las máquinas conocidas para nuestras conexiones.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/contenido_know_hosts.png?token=2b9e6d14107346ffbfa99c2cf73570b684c60353)

Ahora en el servidor veremos que tenemos unos archivos de claves en la ubicaciones "/etc/ssh" que serán utilizados para la validación de claves en las conexiones ssh.
Para que nuestra conexión se realice con un par de claves rsa, una pública que irá a nuestros clientes y otra privada del servidor, deberemos modificar el documento "sshd_config".

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/archivos_key_ssh.png?token=612853995ae19782a95a82ca534fe892602ee1e8)

Pero solo descomentaremos la línea "/etc/ssh/ssh_hosts_rsa_key", con esto nuestras validaciones se harán con el algoritmo de encriptación rsa.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/modificar_sshd_config_server.png?token=30af29058cab445c16cc068d24d496735aad0039)

Ahora generaremos una nueva clave y la sobreescribiremos a la anterior con el comando ssh-keygen y la ruta del archivo, y reiniciamos el servicio ssh.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/ssh_keygen.png?token=11ea93d8caa0ba47d48bbe1b88e66d9ae4bbb3c5)

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/reiniciar_comprobar_sshd.png?token=dd4a95ac0fbd587783d9bd92b5599e2925e324df)

El inconveniente es que la próxima vez que iniciemos sesión desde el cliente de openSUSE nos saldrá un error debido a que cambiamos la clave rsa:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/shh_key_error_conect.png?token=cfe7a18fb7b09c66850844903e6f876af4ffccab)

La solución ante esto es pegar la clave rsa.pub del ssh-server en el fichero de know_hosts de la máquina cliente, veamos:

Copiamos esta clave desde el ssh-server:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/ssh_key_pub.png?token=b731a72a380019d5f31822c894085efd1fea7268)

Y la pegamos en este fichero en el ssh-client1 sobrescribiendo su anterior contenido:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/contenido_know_hosts.png?token=2b9e6d14107346ffbfa99c2cf73570b684c60353)

*(En Windows 7 no hay que modificar ningún fichero ya que la clave se regenera automáticamente.)*

Accedemos nuevamente al usuario apellido1 y en su HOME nos encontraremos el fichero oculto .bashrc que es el encargado de controlar el prompt del usuario.
lo modificamos añadiendo las líneas que se encuentran a continuación y observaremos que cambia:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/fichero_bashrc.png?token=8abef55e306e4557e09bf702ea515863b72ed271)

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/apariencia_prompt.png?token=5d3f0784833a45ca24d59bb332b2c6eb18578d33)

Además crearemos un fichero denominado .alias en el que añadiremos las abreviaciones de comandos para ahorrar tiempo.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/fichero_alias.png?token=6e446c760da0def83364b44959e5d299698360ad)

Una vez llegados a este punto, nos intentaremos conectar al usuario apellido4 desde el cliente para ello desde el cliente con su usuario normal, haremos uso del comando ssh-keygen.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/ssh_keygen_client1.png?token=9430f12bfad20b5b3c0642022ca1e0a06d48e622)

Ahora en el usuario apellido4 crearemos una carpeta en su HOME denominada .ssh y dentro de ella un fichero llamado "know_hosts".
*(Es parecido a lo que realizamos antes pero de forma inversa)*

Y copiaremos la clave rsa.pub a este fichero desde la máquina cliente haciendo uso del comando de copiado seguro scp:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/clave_pub_client1.png?token=cf93dae63845f35b8de9e284cf1829fe20334d26)

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/copiar_archivo_via_scp.png?token=c32f762d8b150b5fd2d295838ba44f7f40d59ddc)

Como podemos ver en la última imagen, tuvimos que introducir la password del usuario para poder acceder a él de forma remota.
Pues lo que acabamos de realizar sirve para eliminar precisamente eso, por lo que al conectarnos otra vez no nos lo debería de pedir:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/d3567580438d5a0345ba55cd489e5f8f7441fafa/trim1/ud2/ssh/images/conectar_directamente_usu4.png?token=3da4e18224be2abd3345dcde4e31ab538b4e748c)

Podemos observar que ahora no nos ha pedido la contraseña del usuario, por lo que se ha realizado correctamente.

El siguiente paso se tratará de ejecutar un aplicación que esté instalada en el servidor desde el openSUSE cliente.
Para ello deberemos descargar dicha aplicación en el servidor, por ejemplo: Geany:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/instalar_geany.png?token=95dafdffac671cba0df4841aa6833bef82612ff7)

Y modificamos el documento "sshd_config" para permitir a los clientes poder ejecutar aplicaciones de forma remota, para ello descomentamos la línea "X11Forwading yes":

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/forwarding_yes.png?token=51be76a139ca814b40dbc5313bfd8be11a017557)

Ahora vamos al openSUSE cliente y nos conectamos por vía ssh al usuario apellido1 por ejemplo. Pero debemos tener en cuenta que para poder tener permisos de ejecución debemos añadir en el comando ssh el atributo -X.
Y una vez dentro, en la consola introducimos el nombre del programa para que se ejecute.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/abrir_geany_remoto.png?token=78e944bf6ed71d16356292a92bd64bda4fc4f360)

Ya que hemos ejecutado el Geany que es multiplataforma de forma remota usando dos máquinas SUSE, ahora probemos con aplicaciones nativas de Windows.
Para ello, desde el servidor SUSE descargaremos el programa Wine.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/instalar_wine_server.png?token=620753ad1229756c3c1d2ef7bd86566537dbc51f)

Y a continuación probamos que realmente funcione.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/wine_notepad_suse_server.png?token=0d6f7c52b6e2f5ac226174ec8f308ac0ef46a6a0)

Una vez comprobado, vamos a la máquina cliente y realizamos los mismo pasos que hicimos con geany, solo deberemos cambiar el nombre del programa que deseamos ejecutar "wine":

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/wine_notepad_suse_client.png?token=eead09f626b97bd00da8de785d9a58894aae69eb)

Y como podemos observar también se ejecuta sin problema.

*En esta última fase, añadiremos restricciones en nuestro servicio ssh, la primera la hemos aplicado ya, que consistía en acceder al usuario apellido1 sin restricción alguna.*

Ahora añadiremos otra que se basará en que no se puede acceder al usuario apellido2 de forma remota, ni aunque pongamos correctamente la contraseña del mismo.

Para ello nos dirigimos nuevamente al fichero "sshd_config" y añadimos la siguiente línea (*preferiblemente al final del documento*):

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/denegar_acceso_usu2.png?token=acf7fe3980680dfb7cc2807f6e6bb15612cc93dd)

Ahora al intentar entrar de forma remota al usuario apellido2 desde cualquiera de los clientes no nos dejaría entrar:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/acceso_denegado_usu2.png?token=2624de56663ba037c8d980b22bb716a4741c10e1)

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/usu2_putty.png?token=c272b89e2cb5dbd23cd61d5ab583f1fd7b21b04c)

Ahora crearemos una última restricción que se tratará en que solo usuario que este en el nuevo grupo remoteapps podrá ejecutar la aplicación Geany.

Entonces creamos el grupo remoteapps y añadimos el usuario apellido4 a este grupo:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/a%C3%B1adir_usu4_remoteapps.png?token=506c1157f3a8925ca50953380835d31880fefe73)

Y por supuesto modificamos los permisos de la carpeta en la que se encuentra Geany: "/usr/bin/geany", de forma que tenga permisos lectura y ejecución el grupo propietario, y le asignaremos el grupo propietario "remoteapps" y reiniciamos el servicio ssh:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/modificar_carpeta_geany_permisos.png?token=cb59934c4aea553f1abb2eedc40481cb6460bdbb)

Desde la máquina cliente SUSE comprobamos que desde otro usuario no se puede acceder al programa Geany.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/acceso_geany_usu1_denegado.png?token=c4d5c230e2db4ddeea30220c8a6354fbb0f0dbc0)

Y de forma local tampoco nos debería dejar.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/error_entrar_geany_usu1_server.png?token=c5adf9e6fa02146ae75fcd517585d33052ecc7ca)

Ahora intentémoslo de forma local desde el usuario apellido4 que está dentro del el grupo autorizado.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/entrar_usu4_geany_server.png?token=6252cf0a2a574cca4a1ee9c843876305c28c9ef5)

Y por último de forma remota...

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/f10811a9b3790adc5b3938853f783ab2af9ab2a3/trim1/ud2/ssh/images/abrir_geany_usu4.png?token=65f7423ca554eb4f07a39b92572d00bf20c337dc)

Y llegados a este punto ya tenemos nuestras restricciones realizadas.