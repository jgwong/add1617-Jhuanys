Jhuanys Gil Wong.
2º ASIR


----------


#Acceso remoto NFS

###Windows 2012 Server

Para habilitar el acceso remoto mediante el protocolo NFS en Windows Server 2012, deberemos agregar un nuevo rol.

Para ello vamos a "Administrador del servidor > Administrar > Agregar roles y características":

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/agregar_rol.png?token=26aa16a1b5e289a914e7c03581ba16e12c04456e)

En esta ventana daremos a *continuar* hasta que nos pida elegir el rol que deseamos agregar, en este caso **Servidor para NFS**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/agregar_servicio_nfs.png?token=c936b12ee07bcec361c64bf1a6e01fbee4f81b22)

Y confirmamos su instalación.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/instalar_rol.png?token=70548f3fe61499844fe2eb96c7b166f40cc7f427)

Una vez instalado este servicio, procederemos a la creación de una carpeta denominada **export11** en la ruta "C:\", y en su interior las carpetas **public** y **private**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/crear_carpetas_export.png?token=81a274fdf1cea08a3637b84f1193671c8fd47a26)

Para compartir estas carpetas mediante NFS necesitamops modificar sus propiedades e ir a "Uso compartido de NFS > Administrar uso compartido NFS".

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/compartir_nfs.png?token=cc62dabcdfadac155397b015e328dd69aed3cf96)

Entonces en la carpeta **public** daremos permisos de lectura y escritura.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/permisos_carpeta_public.png?token=be1fe436983687757d566a5b077915ebaaa7e7cf)

Y en la carpeta **private** solo permisos de lectura.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/permisos_carpeta_private.png?token=a59681b0e9253f1759cd84ec8176b7760919de85)

###openSUSE server

Ahora que hemos finalizado la configuración con Windows Server, configuraremos un servidor openSUSE.

Lo primero será asignar su IP y nombre de host como se muestra en la siguiente imagen:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/ip_suse_server.png?token=4bc6867c35e4a94c1e69671959f349240ee32934)

Una vez realizadas las configuraciones básicas, instalaremos el paquete **nfs-server** desde los repositorios SUSE.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/instalar_nfs_suse_server.png?token=4116e7fcdaf6ae60bbcfcf57f268665e6ff166b9)

Y crearemos sus debidas carpetas compartidas.

En este caso deberemos modificar sus permisos y propietarios por entorno comandos.

La carpeta **public** tendrá permisos estándar y la carpeta **private** permisos 770.

Ambas carpetas tendrán de usuarios propietario **nobody** y de grupo propietario **nogroup**:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/permisos_carpetas_suse_server.png?token=1aac12dd1978ac16e3269321592db4437b46a710)

Ahora deberemos hacerlas carpetas compartidas, para ello nos dirigimos a el fichero de configuración ubicado en **/etc/exports**.

Y añadimos las dos últimas líneas que se muestran en la imagen.

En la primera linea nos viene a decir que permite la lectura y escritura dentro de la carpeta **public**; y en la segunda línea solo lectura dentro de **private**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/fichero_permisos_nfs_suse_server.png?token=2edbde990c9a9d412ccc2e00daebfea5c2ad789f)

Lo siguiente será iniciar el servicio NFS servidor por entorno gráficos en la herramienta YAST:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/habilitar_para_showmount.png?token=f8343ef6b617313925fe18ae8dd61f18b9b9670c)

Ahora que hemos realizado todas las configuraciones respectivas de ambos servidores, con el comando "showmount" mostraremos si realmente las carpetas **public** y **private** está siendo compartidas.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/comando_showmount.png?token=bb7d45af63df9b0d3bfe248d83d3df9ed5a745de)



![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/showmount_windows_server.png?token=1d7367791e172aad447117aa4d83b8c69d3ac4f0)

###openSUSE cliente

Por último configuraremos la máquina cliente que se conectará a estas carpetas de forma remota.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/ip_suse_client.png?token=7455e40a0282f18d0f694bdf33e8ca5421bfcfb2)

Hacemos ping al servidor para comprobar que efectivamente podemos alcanzarle:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/ping_client_suse_server.png?token=dc522a678a3d88c2eda344038a4fe73d455c7fde)

Y comprobamos también desde el cliente que las carpetas están siendo compartidas.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/showmount_client_suse_server.png?token=b1caf46f503a84018f38d7a814d7f6ed77827629)

Ahora crearemos una carpeta denominada **remoto11** para alojar las carpetas remotas del openSUSE servidor y **remoto11_windows** para el Windows Server.

Ambas carpetas tendrán dos subcarpetas (**public** y **private**).

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/crear_carpetas_client.png?token=9d9deb091f44f51d322aa82cb597676ef28f224a)

A continuación las montamos remotamente una a una con el comando "mount.nfs" acompañado de la ip de la máquina emisora y la ubicación de la carpeta en remoto, además deberemos especificar donde deseamos que se monte.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/montar_remoto_public.png?token=a4584a0d8f8550ef45698b2b86105783d2b0d3d3)



![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/mount_remoto_private.png?token=78d2137b4331aa59e387ce5cbfe7a05545cb8d15)

Y con el comando netstat -ntap podemos ver las conexiones activas, en este caso se puede observar la ip "172.18.11.31" correspondiente a la ip de la máquina openSUSE client.

(*La ip de la máquina Windows Server no aparece ya que la captura fue tomada antes de su montaje*)

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/comprobacion_netstat_cliente.png?token=7f976d9d0061e399fb70e629044f9b95f1ab24ae)



![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/montar_remotas_suse_windows.png?token=a8f7114c7fdba1d29328bee9fdee13236dbb0f0d)

Pero como sabemos... Cada vez que reiniciemos o apaguemos la máquina cliente, estos vínculos se romperán y tendremos que montarás nuevamente.

Para ello, iremos a la herramienta "Particionador" y en la sección "Sistema de archivos en red NFS" nos aparecerán los recursos montados actualmente.

Solo deberemos escribir en el apartado "opciones" de cada montaje la palabra **default**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/hacer_permanente_particionador.png?token=d68ab090829fef316217c4c1bcdb77a34c38b918)

Así quedaría finalmente:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/todos_directorios_remotos_creados.png?token=66746ea9a9a58a63cd1fbb411977ee0a86062e3a)

Y como podemos ver (por entorno comandos) las unidades están montadas.

Y una vez se reinicie el equipo seguirán estando de la misma manera.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/montadas_ambas_carpetas.png?token=5746d2965430edb58b38873639a00994fc20a884)

**¿Nuestro cliente GNU/Linux NFS puede acceder al servidor Windows NFS? Comprobarlo.**

Sí, puede acceder.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0acdcb72a89d8a3e409b83f33ce7653c8bc3fa85/trim1/ud3/nfs/images/todos_directorios_remotos_creados.png?token=66746ea9a9a58a63cd1fbb411977ee0a86062e3a)

**Fijarse en los valores de usuarios propietario y grupo propietario de los ficheros que se guardan en el servidor NFS, cuando los creamos desde una conexión cliente NFS.**

Se le asignarán los permisos de acuerdo al usuario y grupo con el que se creó desde el cliente de forma remota.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/aac46f0393b35efd08ab2091934fe5937a904e4a/trim1/ud3/nfs/images/permisos_desde_remoto.png?token=ad75c9c9963afcca7de70e4ec15e6f0a3591cc17)