Jhuanys Gil Wong.
2º ASIR


----------


#LDAP en openSUSE

##openSUSE servidor

Lo primero que deberemos hacer en esta práctica será cambiar el nombre de host del servidor a *ldap-server11*. Y ubicarlos en el dominio *curso1617*.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/nombre_server.png?token=e59c71aaa4a92873f8651c547eee1d503f70d72e)

Además deberemos añadir el nombre de host del futuro cliente al archivo de hosts y asignaremos dos IP's más que servirán de loopback *(mediante yast o entorno comando)*.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/hosts_127.png?token=3f289c9eaa7b2ff14b07291464d3a8e8a8676f7f)

A continuación descargaremos el paquete **auth-server** que servirá para autenticar nuestro acceso.

Para descargarlo haremos uso de YAST:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/instalar_auth_server.png?token=9ea0d0427d7d06a57dc8161ebda14485bbe7cab0)

Continuamos los pasos hasta completar la instalación...

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/auth_server_instalado.png?token=08f93bab31eef8d5e2735ddc25678e4cd141c067)

Una vez instalado, cerramos la ventana de YAST y la volvemos a abrir.

Entonces escribimos **Authentication Server** y ejecutamos esta opción:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/authenticacion_server.png?token=03c8ee7c6d8a9786aaff5472fb8c345b6e61b7d4)

Al iniciarlo nos pedirá que instalemos estos tres paquetes que se muestra en la imagen, seleccionamos *Instalar*.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/instalar_modulos.png?token=4dc5f5b709e4d03b65c3a4c5d85d5e4490a716e9)

A continuación nos guiará por un proceso por el cual seleccionaremos las distintas opciones de configuración del servicio LDAP.

Activaremos el servicio:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/activar_ldap.png?token=14e8914f12ce3cd241330655dcb5f9d4cb78b959)

Establecemos un servidor tipo autónomo:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/servidor_autonomo.png?token=a213443d72c6e0247f3f04c737a89a7c7fd5fe38)

En esta ventana lo dejamos por defecto:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/tls.png?token=ebe303cbf76482f288282643f8889036ff4981c8)

Configuramos la base de datos LDAP, en ella establecemos parámetros como el nombre de la base de datos, su administrador y la contraseña de la misma.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/config_bd.png?token=21066ce2e3ad45a4fa8e9ffb6801de44870d8793)

Deshabilitamos la autenticación Kerberos:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/kerberos_no.png?token=34cbc75417b0c0ed4a7283473b97f7c534723a3c)

Y por último aplicamos la configuración:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/instalar_config_ldap.png?token=666199f46a0d48228394a07a6b69cea95ef5e000)

Una vez finalizada la configuración, comprobamos que el servicio ldap está activo.

Para ello introducimos en la consola de comandos **systemctl status slapd**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/servicio_ldap.png?token=fccbef05c6265d511f4b4ac0eb70944a0a6933f8)

Y comprobamos con el comando **namp** que el servicio ldap está a la escucha y su puerto determinado para ello.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/nmap.png?token=4a7cebfc7e08679ce8af7454af4abe2ac845f66d)

Ahora comprobamos que la base de datos anterior está bien configurada introduciendo el comando **slapcat**:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/slapcat.png?token=351dc555d408c6048c031632ec2890f9f3a89220)

E instalaremos un últimos paquete en la máquina servidor, denominado GQ, que sirve para explorar y administrar nuestro servicio LDAP.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/instalar_gq.png?token=de451e59651f52c726dd7365e4c35a7f7f0d4247)

Ahora creamos dos usuarios nuevos (**pirata21 y pirata 22**) dentro del grupo **piratas**.

Para ello nos dirigimos a la ubicación donde habitualmente creamos usuarios para el sistema, pero seleccionamos **"Definir Filtro"** y seleccionamos LDAP.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/crear_usus_ldap.png?token=02f9017820261c5f60bab490f416d2a1b747ca58)

Y una vez hayamos aplicado el filtro, los creamos como si de usuarios normales se trataran.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/usus_grupo_creados.png?token=c4f2cecf6023e2fed2b87de792b7436600622f85)

Una vez instalado, lo abrimos y podemos observar los datos de la base de datos de nuestro LDAP, así como los grupos de usuarios y usuarios.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/group_people_gq.png?token=c95d8f1a307b211cd020802726ca4507d1d6f7c4)

##openSUSE cliente

En la máquina cliente, lo primero que haremos será configurar el documento **"/etc/hosts"** tal como lo hicimos anteriormente con el servidor.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/hosts_client.png?token=a7eba3a9681a649055542bc2e7c99439f53602f8)

Instalamos el paquete **auth-client** en YAST, que servirá para acceder de forma remota a nuestro servidor vía LDAP:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/auth_client_instalar.png?token=59ecf258051241b2e1f4b89003a9317f91186e5d)


![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/auth_server_instalado.png?token=08f93bab31eef8d5e2735ddc25678e4cd141c067)

Instalamos el paquete GQ también en el cliente, y lo ejecutamos.

Nos dirigimos a **File > Preferences > Server > New Server** y en él, añadimos los parámetros de nuestro servidor en cada uno de los campos:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/preferencias_gq_client.png?token=3707bd0e2de924a78fbebf6112d1b56b18f9b19f)

Ahora si nos dirigimos a Browse podemos observar la base de datos de nuestro servidor con su conjunto de usuarios, *(los usuarios y grupos estarán en ou=people y ou=groups)*.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/gq_client_correcto.png?token=fd285069796df9b2989e980c64cf2dde2690c39b)

Por último, nos dirigimos a **YAST > Authentication Client** y configuramos estos últimos parámetros de conexión.

Añadiremos un nuevo dominio denominado **LDAP** en **sssd**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/sssd.png?token=1067e29eacd5a41c2576547ca7895d9ac6424528)

Ahora crearemos un nuevo dominio en este caso **jhuanys11** con ambos proveedores **ldap**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/sssd2.png?token=7e9f678a6eb716992a765cb2a0a985c2d155adf7)

Y para finalizar editamos nuestro nuevo dominio añadiendo la base de datos que deberá usar dicho dominio.

Estos cambios se guardarán en el archivo **/etc/sssd/sssd.conf**

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/4b9d1bb3468dd9f7f4046f92c22bc83c4b9877d5/trim1/ud3/ldap/images/sssd3.png?token=90b37543381a08a55c3a6fccd0e1b6bc70c9b988)