Jhuanys Gil Wong.
2º ASIR


----------


#Acceso remoto Samba


Para la realización de esta práctica necesitaremos dos máquinas OPENsuse (una servidor y otra cliente) y una máquina Windows 7 que actuará como cliente.

El primer paso será configurar la IP en las 3 máquinas y nombre de host, en la siguiente imagen podemos ver como quedaría la configuración con la máquina servidor:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/comprobaciones_server.png?token=f85528207043400a87a6a9cb46df0d8669c51d4d)

###Trabajando en el servidor

Una vez hayamos configurado las IP, deberemos crear en el servidor tres grupos: jedis, siths y starwars.

Dentro del grupo de los jedis irán los usuarios jedi1 y jedi2; en el grupo de los siths los usuarios sith1 y sith2; y dentro del grupo starwars incluirlos a todos.

Además crearemos un usuario denominado supersamba que estará en todos los grupos y otro llamado smbguest solo en el grupo starwars.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/usuarios_en_grupos.png?token=e4df26edd4cf1fbbaecd4af8a62cbaa2f58ca1fb)

Al usuario smbguest no será posible entrar, para ello deberemos modificar el documento de texto "/etc/passwd" y en la línea "smbguest" cambiar al valor "false" como se muestra en la imagen:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/modificar_smbguest.png?token=f38c00f521d3613f691ffb2f80cf08cb8102c2d5)

Ahora creamos las carpetas para los grupos de usuarios en la ruta "/srv/samba11".

A la carpeta "tatooine.d" solo podrá acceder su grupo "jedis" y su propietario "supersamba", los mismo con "corusant.d" pero su grupo será "siths".

En cambio a la carpeta "public.d" podrán acceder todos los usuarios del grupo "starwars" , su propietario "supersamba" y otros usuarios con permisos de lectura y ejecución.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/carpetas_samba_propietarios.png?token=4290da3f434502b4f1da09267b110091124014e9)

Ahora deberemos proceder a instalar el servicio samba desde los repositorios openSUSE, para ello introducimos el comando "zypper install samba":

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/instalar_samba.png?token=90659084f6d1cd1ded14db14be4332e305859b8e)

Una vez instalado nos dirigimos al archivo "smb.conf" em la ruta "/srv/samba11" y hacemos un copia de este documento para posteriormente modificarlo.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/copia_archivo_samba.png?token=8c16c1f663b75a564f451cf06ccedab5f66b6c01)

Entramos en el documento y modificamos sus parámetros para indicarle al servicio cual es su grupo de trabajo, el nombre del servidor y las ubicaciones de las carpetas sujetas a este servicio.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/fichero_samba_modificado.png?token=7a9a4801c419e8817dcc176e0741a4a8517f74e2)

Para comprobar que esté correctamente configurado, nos dirigimos a la consola e introducimos el comando "testparm".

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/testparm.png?token=acd811067c17a409d5ef076e540f7083581a3020)

Ahora crearemos una clave samba para los usuarios que anteriormente creados:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/anadir_usu_a_samba.png?token=9c6177973bdc1b314e545d0bf86c02b3312aeb06)

Y comprobamos que efectivamente se hayan creado en el paso anterior:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/comprobar_usuarios_creados_samba.png?token=66195220c2c344ae26fda9009270439e53e373b9)

Ahora que hemos terminado con la configuración del servidor deberemos comprobar si el servicio "smbd" está a la escucha.
Podemos observar que se encuentra en el puerto 1662:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/puerto_escucha_samba.png?token=c1428033b4883880e4fc1f0e863b64f49380326c)

###Cliente de Windows 7

Lo primero que deberemos hacer en la máquina Windows 7 será configurar el nombre de la máquina, en este caso "smb-cli11b" y reiniciamos el equipo.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/cambiar_nombre_windows.png?token=12d0e5b642e0ffc1af8e597c2c120640d2adb1c5)

A continuación  nos dirigimos al explorar de archivos y en la sección "Red" introducimos la IP del servidor openSUSE en la barra de búsqueda.

Por ejemplo:  \\192.168.1.1

Y observaremos que se nos muestran las carpetas que hemos creado anteriormente en el servidor.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/acceder_remoto_cli11b.png?token=0dffb9b35273531012456bdbe8b9ff15d3c0b273)

Ahora intentamos acceder a las carpetas con cada usuario, a "corusant" con usuario "sith" y a "tatooine" con un usuario "jedi":

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/acceder_a_carpeta_remota.png?token=fa9ff7f2f0949c757af0dc61f75c6a3cf4cbc803)


![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/dentro_carpeta_grupo_sith.png?token=3ccf937a4d2124d31ec85beb859df726c24b610c)

Si intentamos acceder ahora a un otro usuario samba nos daría un error porque no hemos cerrado la conexión remota con el usuario "sith1".

Para ello iremos a la consola de comandos "cmd" e introduciremos el comando "net use * /d /y" para cerrar todas las conexiones remota existentes.

Y accedemos a la carpeta "tatooine":

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/cerrar_conexion_windows.png?token=08df57615c47b2fdf417e4aba0f92db404d06a1b)


![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/dentro_carpeta_grupo_jedi.png?token=c245f00a332b59132b79eabbdb5304ad41215ea8)

Nuevamente cerramos todas las conexiones remotas e intamos acceder la carpeta "public" sin usuario.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/acceder_public_invitado.png?token=b1b7accc320a1654ef19950c5ae016077a6d90ca)

Ahora nos dirigimos al servidor samba e introdumos los siguientes comandos para comprobar el estado del servicio y los usuarios que se están utilizando.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/comprobaciones_smb.png?token=fa860fba0916d9aa33b2f9cb43e384fdbc557181)


![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/comprobaciones_smb2.png?token=bd5018a2e0af1c78de4d32e503bed793913aa0e1)

Y en Windows 7 introducimos el comando "netstat -n" para comprobar las conexiones activas.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/comprobaciones_smb3_windows.png?token=d4fddd495453f0e2e3e65638e89ec416de7cfd89)

Cerramos todas las conexiones remotas...

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/comandos_net_use.png?token=6e019b81dafb327fe0b651cb15b9479d568f43af)

Si deseamos consultar la ayuda del comando anterior deberemos emplear  el atributo "/?".

Y el comando "net view" para mostrar todas las máquina accesibles en la red local.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/comandos_net_use2.png?token=e92b9fd004bf0bdae44942828c2c2fa8c5158114)

También podemos acceder de forma remota por entorno comandos empleando nuevamente el comando "net use":

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/comandos_net_use3.png?token=4f41aacbabeaec62211478f6fb6ed0457e7c956f)

Y montar una unidad de almacenamiento denominada S en la carpeta que deseemos, en este caso en "public":

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/crear_montaje_automatico.png?token=329ab4d89f4ab74b09385f6957ee2952f7cd5f10)


![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/crear_montaje_automatico2.png?token=031c33d768a16328858c82bd93c08688a3e45cb9)

Introducimos nuevamente los comandos de comprobación en el servidor y observamos que el usuario "smbguest" está abierto debido a que utilizamos la carpeta "public":

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/comprobaciones_smbp2.png?token=944bf84e690647fd10be66ab6bc8c5df7df34bcf)


![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/comprobaciones_smbp2_2.png?token=4f584701fbcafc7fda9244ac8b961fd2401ae52a)

Y el comando "netstat -n" para ver la conexiones abiertas en el Windows 7.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/comprobaciones_smbp2_3.png?token=4ce77bccb594eb29e41120ba1c86c1b2f069a964)

###openSUSE cliente

En la máquina cliente openSUSE nos dirigiremos al gestor de archivos e introduciremos en la barra de búsqueda "smb://172.18.11.31" para conectar remotamente con el servidor.

![enter image description here](https://bitbucket.org/YuaUHD4K/add1617-jhuanys/raw/0656b94e4751658b69620004b3441536b64da59f/trim1/ud3/samba/images/gestor_archivos_client)

Y accedemos a las carpetas "corusant" y "tatooine" como hicimos en Windows 7.

![enter image description here](https://bitbucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/gestor_archivos_client)

Una vez dentro de la carpeta creamos un documento .txt y una carpeta para comprobar que tienen permiso de escritura.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/entrar_corusant_suse_client.png?token=8b12dddcbb3f2f6a28108de2a3cff9d9885ef846)


![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/corusant_entrar_suse_client.png?token=674ecc60733809451b796e62780efec594a48f5f)


![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/tatooine_suse_client.png?token=0afdb06f1b42e540038f7bfedfb4f71ff04dedb3)


![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/tatooine_entrar_suse_client.png?token=1f2c42269cd13c4bba84a9a07e189f4e8b79c19e)

Accedemos a la carpeta "public" y al intentar crear un archivo dentro de ella nos saldrá un error porque no permite escribir dentro de ella.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/0656b94e4751658b69620004b3441536b64da59f/trim1/ud3/samba/images/intentar_escribir_public.png?token=bf6adf18890f171b424bbacc11a56bde2f36fcc4)

Y volvemos a introducir los comandos de comprobación en el servidor para observar todos los usuarios que están siendo utilizados remotamente.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/comandos_comprobacion_suse_server.png?token=94e130274799c7dacd146f618ef95c99f60d926f)

Y desde el cliente el comando "netstat -n" para comprobar las conexiones abiertas.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/comandos_comprobacion_suse_client.png?token=dcb0631c714bae7f2dee5f9f1c6d290c741fa57f)

Desde el cliente, introducimos el comando "smbtree" y nos mostrará una lista de las máquinas accesibles que están ubicadas en nuestra LAN.

*(Puede NO aparecer nuestra máquina servidor)*

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/smbtree_client.png?token=09cb724fb364f459c3f95a9ecf8aa5445e252498)

Nos conectamos a nuestro servidor aunque no sea visible con el comando "smbclient --list y su ip".

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/smbclient_client_suse.png?token=45127a287918cb0edd4a591b61f314286dc23d46)

Ahora montaremos una unidad de almacenamiento en la ubicación "/mnt/samba11-remoto/corusant".

Para ello creamos dichas carpetas.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/crear_carpeta_mnt_samba_corusant.png?token=63a591cee241907ce6afa0ec4f13c516614d5837)

Y con el comando "mount" acompoñado de la ip de la máquina que contiene la carpeta en remoto y el usuario que puede acceder a ella, podremos añadirla.

Para comprobar que está correctamente montada, con el comando "df -hT" nos mostrará el dispositivo y su ubicación *(Se puede observar en la última línea)*

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/montar_corusant_server_en_remoto.png?token=312fb4783fc99241221caac594175fb6c24e66ba)

Introducimos los comandos de comprobación en el servidor para ver el estado del servicio y las conexiones remotas abiertas...

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/ultimas_comprobaciones_server.png?token=c22402e9d790dc8a39d8e7699bcb3b4abe594c38)

Y nuevamente el comando "netstat -n" en el cliente openSUSE.
![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/ultimas_comprobaciones_client_suse.png?token=c2e342a2f1afb840ce058e790bbc02de67b82236)

Para que este enlace sea permanente, nos dirigimos al documento de texto "fstab" ubicado en la carpeta "/etc".

Y añadimos la última línea que se muestra en la imagen:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/6e9e7c79d0e7bcb740ed78c4bc52c72ce62575a7/trim1/ud3/samba/images/fichero_mount_perma_client.png?token=0c4951f7d43925039c059a9e810b597f03be88d9)


###¿Las claves de los usuarios en GNU/Linux deben ser las mismas que las que usa Samba?

Las contraseñas pueden ser diferentes.

###¿Puedo definir un usuario en Samba llamado sith3, y que no exista como usuario del sistema?

Primero se debe de crear el usuario en el sistema para poderlo usar en el servicio Samba.

###¿Cómo podemos hacer que los usuarios sith1 y sith2 no puedan acceder al sistema pero sí al samba? (Consultar /etc/passwd)

Se deberá cambiar en este fichero en ambos usuario la linea a /bin/false.
