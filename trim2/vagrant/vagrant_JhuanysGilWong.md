Jhuanys Gil Wong.
2º ASIR


----------


#Vagrant

Lo primero que deberemos hacer para realizar esta práctica será tener instalado el último paquete del servicio vagrant.

Una vez tengamos el servicio actualizado, crearemos una carpeta denominada **mivagrant11** dentro de la carpeta "Documentos" e iniciar el servicio vagrant dentro de esta nueva carpeta.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/carpeta_vagrant.png?token=21cc3b5a18a51f3635d5590840d96a7a4b61430b)

Ahora deberemos añadir una nueva caja a partir de el enlace de descarga de **Ubuntu precise 32**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/hacer_caja.png?token=986fe88d51c98a7226cb3284e0898efe75ca57f4)

Podremos ver con el comando "vagrant box list" que efectivamente se ha creado una nueva caja.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/mostrar_caja.png?token=4d5383dd53b4fbb136778996502bcb5392c0e995)

Y modificaremos el archivo **Vagrantfile** para que ejecute esta caja al iniciar nuestra máquina vagrant.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/conf_vagrantfile.png?token=cc8af20bc593b3aa5098559598d15b455e5962c6)

A continuación iniciaremos nuestra máquina con el comando "vagrant up", equivalente a "iniciar" en VirtualBox.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/iniciar_vagrant.png?token=5c67657c897d43d76488b344578c9481d1681899)

Ahora accederemos vía ssh a nuestra máquina para comprobar que efectivamente es accesible:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/ssh_vagrant.png?token=246be1396274f2d4d73ba0831c81d0471bad1210)

Una vez dentro de nuestra máquina **Ubuntu precise 32**, mostraremos los archivos de la carpeta **/vagrant/** como se muestra en la imagen:

Esto nos mostrará las carpetas compartidas de nuestra máquina vagrant.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/carpeta_compartida.png?token=a953dbe6bae81ad802cadca5cef17ad2bf7cd1a9)

Apagamos la máquina con el comando "vagrant halt" y nos dirigimos a modificar sus puertos de escucha en el archivo **Vagrantfile**.

De modo que se acceda a "localhost" por el puerto "4567".

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/puerto_vagrant.png?token=17cb56332e62ab8f9b22707be7d09035a1c8d876)

Iniciamos nuevamente la máquina vagrant *("vagrant up")* y comprobamos que esté el servicio activo en el puerto que hemos establecido.

(*Este comando debe realizarse desde la máquina real*).

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/comprobacion_puerto.png?token=a108bd0ef80b34ad52933db410292a69e3d953de)

A continuación en nuestra máquina virtual instalaremos el paquete apache2 desde los repositorios de Ubuntu.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/instalar_apache.png?token=d0654244d119def2469ee7bffdae9e8032ace652)

Y una vez instalado, accediendo en el navegador a la ruta "127.0.0.1:4567", nos mostrará la página por defecto.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/vagrant_works.png?token=0b381bff224ec93412cfd7d2ee786e6fabc6e25f)

Ahora, crearemos un archivo de autoinstalación de nuestra máquina vagrant.

Para ello apagaremos y eliminaremos la máquina con el comando "vagrant destroy".

Y creamos el archivo **install_apache.sh**. En este archivo irán comandos bash acerca de los pasos realizados anteriormente y la configuración de nuestra página localhost.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/script.png?token=71f3ba8a748a8d2835b9b71eedb8f893f94d8cc0)

Ahora modificamos el archivo **Vagrantfile** y añadiremos la línea  **config.vm.provision :shell, :path => "install_apache.sh"**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/enlazar_script.png?token=17bc6a8c7e4a4325be941002ae4c49263b093bff)

Ahora instalamos nuevamente nuestra máquina vagrant con el comando "vagrant up" y al acceder a la dirección "127.0.0.1:4567" nos mostrará la configuración del fichero **install_apache.sh**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/vagrant_web_modified.png?token=9a1ad1cd90c2ce636b69156de67e86fd7c92bd6a)

A continuación apagamos la máquina vagrant *("vagrant halt")* y modificamos nuevamente el archivo **Vagrantfile** para añadir una nueva línea de suministro mediante "Puppet".

Puppet se encargará de asegurarse que al instalar nuestra máquina vagrant, esté presente el programa que especifiquemos en este fichero de configuración.

Reinstalamos la máquina...

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/puppet_vagrantfile.png?token=afa027dba2211d0c929b883bce8ade69759e7814)



![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/default_puppet.png?token=02f9081114cb645e6abe34dc28f957a6cbbaa692)



![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/nmap_instalado.png?token=7b605d66fcb5f9c87450109a4e802d7870c13246)

Una vez instalada la máquina, comprobamos que Puppet a cumplido con su trabajo.

Y como se muestra en la imagen, el programa "nmap" que hemos especificado en el fichero de configuración está presente tras la instalación de la máquina. 

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/nmap_ubicacion.png?token=37ec0cd7d50ae88c764e5cf2a5418f8dd22cc3cc)

###Caja personalizada

Creamos una nueva carpeta en "Documentos" denominada **mivagrant11conmicaja**.

Dentro de ella iniciaremos el servicio vagrant con el comando **vagrant init**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/vagrant_nuevo.png?token=6bfb880f89b66922f38237a1cea122b8fd9a29c7)

Ahora seleccionamos una de las máquinas que ya tengamos en VirtualBox y creamos un usuario vagrant con contraseña vagrant.

Además le cambiamos la contraseña al usuario root a vagrant para que la instalación se pueda completar correctamente.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/crear_usu_vagrant.png?token=cde29f57c9457b1bf1e550172b690b5498a5494a)

Crearemos una carpeta oculta denominada ssh con permisos "700" y dentro de ella otra carpeta denominada "authorized_keys" con permisos "600".


![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/permisos_carpetas_ssh.png?token=be1665222f20c7f677aea8260d87bc165ac302df)



![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/cambiar_passwords.png?token=32d4979e01fd74e77fc2be52e41985808e6f829c)

En el archivo de configuración "/etc/sudoers" para que el usuario vagrant posea privilegios y no pida contraseña.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/sudoers.png?token=d8012af14a5ec032a74474076a1faf3646c58725)

Por último, comprobamos que las guest-additions estén instaladas en el sistema, en este caso ya están instaladas.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/guest_additions.png?token=74448245e24f7912a2428fea7b4611570daafc75)

A continuación desde nuestra máquina real creamos la caja con el sistema que hemos modificado anteriormente:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/vagrant_box.png?token=fa64ede7534e9e9e73fe9e79bc007519b09eef45)

Con el comando que se muestra a continuación nos mostrará el tamaño en GB que ocupa nuestra caja.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/peso_package.png?token=d6ec0e7a4d577d820594fcca3518aaa1ae9faf56)

Y la añadimos a la lista de vagrant.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/anadir_a_repositorio.png?token=dd119312b7d04d2259cef3f07d25641355efe45f)

Con el comando "vagrant box list" comprobaremos que se ha añadido correctamente a la lista.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/lista_vagrants.png?token=d5cea5d194b28f60a9ef2f99392c8dc26591373e)

Y por último iniciamos esta nueva máquina con el comando "vagrant up".

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/levantar_vagrant_final.png?token=229d2f7d65c1860ef23f4f7a76e8ccaa8ed74ce3)

Es posible que al iniciarse de problemas con el acceso ssh y se quede reintentando conectar unos minutos.

En mi caso nunca llegó a conectar.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/7e90e9652d39d910b9135263c6c8ad96af61c9f9/trim2/vagrant/images/reintentando.png?token=ca6de1ed384e59004eb88821851f3e6c4e21b0ec)