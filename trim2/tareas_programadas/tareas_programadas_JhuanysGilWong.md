Jhuanys Gil Wong.
2º ASIR


----------


#Tareas programadas

###Windows 7

En Windows, si deseamos programas una tarea a una determinada hora y día deberemos acudir a las **Herramientas administrativas**, dentro del **Panel de control.**

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/pogramador_tareas_windows.png?token=88055e98d9a13a1a7183615c3760354e8945ae99)

Se nos mostrará entonces una ventana en la cual podremos añadir una nueva **Tarea básica** en la barra de menú situada a la derecha.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/tarea_basica.png?token=6e3f4a457ccb6a54ef3d1d476ec9b451b98aca0c)

Para crear una nueva tarea seguiremos una seria de pasos:

En el primer paso deberemos elegir el nombre y descripción de la tarea.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/crear_diferida.png?token=a6f8df9a398ed5613314e4804b01eaf5b9e97c4c)

En el siguiente, elegiremos con la frecuencia que se realizará esta tarea.

Se ejecutará solo una vez debido a que será una ***tarea diferida.**

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/crear_diferida2.png?token=3063483e91bb007bd4ca3f52408f47c33720dce2)

A continuación elegiremos la acción que realizará *(Mostrará un mensaje en pantalla)*.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/crear_diferida4.png?token=23ab9afc175f720ca85d89263d94b11e154a022c)

Y ahora introducimos el mensaje que deseamos mostrar al ejecutarse.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/crear_diferida5.png?token=0df65211fd182905a47235973b9b8969552c2694)

Ahora solo nos queda esperar...

Y como podemos ver llegada la hora, se muestra una ventana emergente mostrándonos el mensaje.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/mostrar_diferida.png?token=0b94c9bc6b4e211c269c54ea9cd91a12a087e061)

En el siguiente paso crearemos una tarea periódica, es decir, se lanzará siempre que llegue a una determinada fecha y hora.

Para ello, seguiremos los pasos anteriores, eligiendo la frecuencia **Diariamente**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/crear_periodica.png?token=6808aaca245131a3ab4f8660eb0fd85bcacc1377)

Ahora ejecutaremos el ejecutable que hace que Windows se apague **shutdown.exe** con los atributos -s *(guardar tareas)*, -t *(tiempo),* -c *(comentario)*.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/crear_periodica3.png?token=97b107cc6f3907056c956e4a23f370fd8353ae03)

Al finalizar la creación de la tarea, esperamos a que llegue su hora programada...

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/mostrar_periodica.png?token=37758c3e02127760da6bb8c181be3695b16c7f6d)

###openSUSE

Para crear tareas en openSUSE deberemos habilitar el **servicio atd** en la herramienta administrativa Yast.

Una vez habilitado, lo iniciamos y comprobamos que esté activo:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/iniciar_atd.png?token=a4ff1e60505739fcfe3caf5a6e03b538c5c894d7)



![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/atd_activo.png?token=b20f0884cc16a64362f8122e538ec26ffb962c2b)

Ahora bien, mostrar un mensaje con esta herramienta deberemos introducir el comando de la primera línea, de este modo se ejecutará dentro de un minuto una vez terminada de configurar dicha **tarea diferida**.

Para escribir que deseamos mostrar emplearemos el comando ***echo*** y, una vez ejecutada nos mostrará la ruta donde se depositó el mensaje:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/diferida_suse.png?token=a4b3c306bbaf88fa05a8ec3c8b390b55e67112ec)

Para crear una tarea periódica utilizaremos otro medio, denominado **contrab**.

Deberemos añadir la nueva tarea modificando el fichero ubicado en la ruta */etc/crontab*.

La tarea que se muestra en la última línea apagará el sistema con el usuario especificado *(root)* a las 13:17 horas de todos los días de todos los meses de todos los años.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/fichero_crontab.png?token=47673ee1059841dc49f8efbcf5fc7d6d9fd5bd5c)

Una vez guardado el fichero y cumplida su fecha de ejecución, nos mostrará el siguiente mensaje advirtiéndonos que el sistema se apagará próximamente.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/b7ed1aa00935e49bb23e2335a77c7a6a846b49df/trim2/tareas_programadas/images/apagar_maquina_crontab.png?token=c56fdf7269472cf40600cc613d44d0a85aa7917c)



