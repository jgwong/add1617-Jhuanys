Jhuanys Gil Wong.
2º ASIR


----------


#Docker

El primer paso para realizar esta práctica será comprobar la versión del kernel de nuestra máquina openSUSE Leap, deberá ser superior a la 3.10.

En este caso es la 4.4:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/version_kernel.png?token=f5a41af9ec97eb94ba2a79f81400f3798a2ea95c)

Ahora instalamos el paquete docker desde los repositorios SUSE.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/install_docker.png?token=c6f6f476b9c4d1697afcadc87dd9e597e75b49f1)

Y una vez terminada la instalación iniciamos la aplicación y comprobamos la versión instalada:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/iniciar_y_version_docker.png?token=607c1768757c30e95918ace74b048644bb6ea2af)

### Descargar un contenedor

Con el comando **docker images** nos mostrará los contenedores que hemos creado, nos aparecerá vacío debido a que aun no hemos creado ninguno.

Pero con el comando **docker run hello-world** conseguiremos descargar y ejecutar un contenedor con la imagen **hello-world**

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/comprobacion_docker_usu.png?token=7a55435bf88220c87ef227ce7872ca4ff505a61b)

Ahora podemos observar que sí tenemos un contenedor creado.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/comprobacion_docker_usu2.png?token=f041a5dfb6a4b0303279f8bf200ffda62875adfa)

En la herramienta YAST modificaremos los parámetros de red para que nuestra tarjeta de red actúe de intermediaria, es decir, los datos que recibe de su puerta de enlace los pueda enviar a una tercera máquina y viceversa. **(Habilitar reenvío IPv4)**

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/reenvio_ipv4.png?token=ea3dab770a49d9fb5a9f47bb6f82726130097e0b)

### Crear un contenedor manualmente

A continuación, buscaremos en los repositorios online de docker la imagen del S.O. debian.

Y descargaremos la versión debian 8.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/docker_pull.png?token=bb0e51bd6d6d8c89d7ee234f5b8d12849abc61f5)

Y añadimos las librerías para openSUSE.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/docker_pull2.png?token=591ca63474ace118cfa30af44baceafa14909a1e)

Ponemos en marcha nuestro contenedor debian y comprobamos que podemos acceder a ella en la ruta **/etc/motd**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/crear_contener_mv_debian.png?token=58c85808a1692a2ed6c06e748adf1544f5c579c2)

Ahora actualizamos los repositorios de nuestro sistema invitado e instalamos **nginx** y el editor de texto **vi**:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/instalaciones_debian.png?token=4a21713416d5395059129795ef54dbb9f847186f)



![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/instalaciones_debian2.png?token=5940738698147663130b45a253d5e69a6ae9f340)

Nos movemos a la carpeta **/usr/sbin/nginx** y comprobamos que el usuario www-data *(usuario nginx)* está presente.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/iniciar_nginx.png?token=56575db4227cea6e37faad9700762e230be211c0)

Creamos un html que se mostrará al acceder a nuestra página web Nginx.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/crear_holamundo.png?token=d453a303f31c53eefa90eca44e043eac34ff58f1)

También crearemos un script que iniciará nuestro contenedor y permanezca sin cerrarse.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/script_server_sh.png?token=bef9785ab0837079427a8503b0ce9b54202f4aef)

Comprobamos que nuestra máquina sigue en la lista de contenedores:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/iniciar_mv_debian.png?token=b814740a655d55fba7433a1cc5c25b087750e5f8)

Y creamos una nueva máquina en la ubicación **jhuanys/nginx**. 

*(El ID del comando es el ID del contenedor anterior)*.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/docker_commit.png?token=137c770ef71eebe8268c390cf375c15c5d58d742)

Detenemos la ejecución de la máquina debian con el comando **docker stop mv_debian** y comprobamos la lista de contenedores.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/parar_mv_debian.png?token=c6001554adc98d62242fbb5b7529d6a6617448e3)

Ahora eliminamos la máquina debian mediante su ID de contenedor con el comando **docker rm**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/eliminar_mv_debian.png?token=bd0351ed732cf3053abcb054e38f6c4036f9802e)

E iniciamos el contenedor que hemos creado anteriormente en la ubicación *jhuanys/nginx*

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/iniciar_docker_nginx.png?token=705cbd5905b22691572c31449a68b0c044230688)

Comprobamos el el contenedor en ejecución y en la columna *PORTS* nos dirá la IP y el puerto para acceder a él desde un navegador.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/iniciar_docker_nginx2.png?token=de825c334cdfcb6edb7cf6de78e86e1b3bec28f9)

Debido a que se está ejecutando en la misma máquina, la IP anterior *(0.0.0.0)* la sustituimos por *localhost* y añadimos el puerto de escucha:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/nginx_web.png?token=2fdebb3559694f6a128b87a8a8d236dd2e17bc1b)

Una vez comprobado que podemos acceder desde un navegador, detenemos la máquina y la eliminamos.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/parar_borrar_mv_nginx.png?token=d43a2179e63bd12149b4159d0f785ce29fe15f9f)

Pero como vemos, aun seguimos tendiendo la imagen de instalación.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/comprobaciones_apartado_6.png?token=676654a652a840cd48e07f77255f58dbbf97d0ef)

###Crear contenedor con Dockerfile

Creamos una carpeta denominada **docker11** y en su interior un archivo de configuración denominado **Dockerfile**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/archivos_docker11.png?token=4877aa4aab4ab7899521e39375c098c2c62addc7)

Será un script de instalación de las aplicaciones **nginx y nano**, además de copiar los archivos **holamundo.html y server.sh** en una ruta específica.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/contenido_dockerfile.png?token=e6c326798a2f17df11da0924ea86164e97c3fd8c)

Y construimos la nueva imagen a partir del archivo **Dockerfile**.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/docker_build.png?token=18938674d0d0636671a334c119d34668a765b0e9)

Comprobamos que se haya añadido la nueva imagen.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/contenedores.png?token=7a852b6f975aa7ad2e4052bbb54416a60629379d)

E instalamos el contenedor.

![](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/iniciar_contenedor2.png?token=061a6ab502597d0595bd2d8b014f6ec25f2d8407)

Realizamos el mismo proceso anterior para acceder a la página web por defecto de Nginx.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/nginx_web2.png?token=d86ace719b84f4aabbc0ebbcac1b8eab675fdae0)

Si deseamos acceder a nuestro documento holamundo.html, lo especificaremos en la barra de direcciones del navegador de la siguiente forma:

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/holamundo_web.png?token=a58f88081e9c09ef834b6b6ce76f4a533c3aacac)

Por último, detenemos el contenedor y lo eliminamos.

![enter image description here](https://bytebucket.org/YuaUHD4K/add1617-jhuanys/raw/e56aad53e1919692f92eb999b9e8d5f9420c4194/trim2/docker/images/parar_destruir_mv_nginx.png?token=4180329e8acb4b7eb12c80190daca2cab73aad7c)



